function UnityProgress(gameInstance, progress) {
  if (!gameInstance.Module)
    return;
  if (false && !gameInstance.logo) {
    gameInstance.logo = document.createElement("div");
    gameInstance.logo.className = "logo " + gameInstance.Module.splashScreenStyle;
    gameInstance.container.appendChild(gameInstance.logo);
  }
  if (!gameInstance.progress) {    
    gameInstance.progress = document.createElement("div");
    gameInstance.progress.className = "progress " + gameInstance.Module.splashScreenStyle;
    gameInstance.progress.empty = document.createElement("div");
    gameInstance.progress.empty.className = "empty";
    gameInstance.progress.appendChild(gameInstance.progress.empty);
    gameInstance.progress.full = document.createElement("div");
    gameInstance.progress.full.className = "full";
    gameInstance.progress.appendChild(gameInstance.progress.full);
    gameInstance.container.appendChild(gameInstance.progress);
  }
  gameInstance.progress.full.style.width = (100 * progress) + "%";
  gameInstance.progress.empty.style.width = (100 * (1 - progress)) + "%";
  if (progress == 1)
  {
    if (gameInstance.logo) 
      gameInstance.logo.style.display = "none";
    if (gameInstance.progress)
      gameInstance.progress.style.display = "none";

    window.onresize = function () {
        var size = gameInstance.container.clientWidth + "x" + gameInstance.container.clientHeight;
        console.log("Sending resize event " + size);
        gameInstance.SendMessage("Game", "OnWindowResize", size);
    };
  }
}